import os
import sys
import shutil
import glob
import time
import colorama
from colorama import Fore, Back, Style

# Make sure the working directory is the same as the script file
os.chdir(os.path.dirname(os.path.realpath(__file__)))

def main():
    # Initialize colorama
    colorama.init(autoreset=True)
    
    all = False
    if len(sys.argv) == 2:
        if sys.argv[1] == "all":
            all = True
        else:
            print "Usage: python update_default_response.py [all]"
            exit(0)

    default_src = "./default_response/feedback/".replace("/", os.sep)
    evals = glob.glob("./reports_workspace/*/feedback/".replace("/", os.sep))

    num_processed = 0
    for eval in evals:
        print(Style.BRIGHT + "Deleting: " + eval)
        if all == True:
            foo = 'y'
        else:
            foo=raw_input('Continue? (y/n): ')
        
        if foo == 'y' or foo == 'Y' or foo == "":
            shutil.rmtree(eval)
            print(Fore.RED + 'Deleted'),
            time.sleep(0.1)
            print('...'),
            shutil.copytree(default_src, eval)
            print(Fore.GREEN + 'Updated')
            num_processed = num_processed + 1
        else:
            print 'Skipped'
        

    print
    print("Total Processed: " + str(num_processed))
    print("Total Reports:   " + str(len(evals)))

main()