import fnmatch
import glob
import os
import shutil
import sys
import zipfile

import progressbar

# Make sure the working directory is the same as the script file
os.chdir(os.path.dirname(os.path.realpath(__file__)))

def cleanUp():
    searches=['evaluations/*.zip'.replace("/", os.sep)]
    for s in searches:
        files=glob.glob(s)
        for f in files:
            os.remove(f)

class CurrentFolder:
    maxl = 12
    i = 1
    txt = "bb"
    def update(self, pbar):
        return (str(self.i) + ". " + self.txt).ljust(self.maxl + 5)

i=1
evals = glob.glob("./reports_workspace/*/evaluation.zip".replace("/", os.sep))

maxlength = max(len(s) for s in evals)
cf = CurrentFolder()
cf.maxl = maxlength
widgets = [cf, progressbar.Bar(), '>>> ',progressbar.Percentage(),' ', progressbar.ETA()]
pbar = progressbar.ProgressBar(widgets=widgets, maxval=len(evals))
# maybe do something
pbar.start()

for eval in evals:
    newFile = eval.strip().split(os.sep)[2] + "(feedback).zip"
    cf.txt = newFile
    cf.i = i
    pbar.update(i)
    shutil.copyfile(eval, "./evaluations/" + newFile)
    i=i+1

print
print
print 'Total: ' + str(len(evals))
