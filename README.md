# Software Engineering Work Report Marking Scripts #

This repository contains scripts to help facilitate and speed up the process of marking the Software Engineer's Work Term Reports.

## Initial steps
 * Ensure python is installed, including 'colorama', 'xlrd', and 'progressbar' packages.
    * `pip install colorama xlrd progressbar`
 * Ensure 'pdflatex' is installed and on path.
 * **Change the TA Marker**'s name in '/default_response/feedback/feedback.tex' (replace all cases of 'Jonathan' and corresponding email)
 * 'templates (do not copy)' folder is not automated; all steps manual. **Change TA marker name** in:
    * Repeat for 'templates (do not copy)/employer_marked_template/employer_marked.tex'
    * Repeat for 'templates (do not copy)/not submitted template/no_submission.tex'
 * Pdflatex these files:
    * `cd default_response/feedback/`
    * `pdflatex feedback.tex`
    * Check that no errors exists



## First step
Create a temporary folder or working directory. Copy everything from this folder into working dir.

~~~~
git clone https://bitbucket.org/jshahen/se-wkrpt-marking-scripts
rm -rf se-marking-scripts/.git
~~~~

## Second step
Download all reports from Learn as a ZIP file (must be pdf format; only one pdf per user).

**Note:** The zip must look like the following:
~~~~
Submissions.zip
|- <file1>.pdf
|- <file2>.pdf
|- <file3>.pdf
|- index.html
~~~~

## Third step: Run 'python setup.py <zip_file>'
This creates the directories:

 * `evaluations/` - This folder will contain all the final feedback files for each student. **DO NOT PUT ANYTHING IN HERE - AUTO POPULATES**
 * `reports_original/` - Stores the original PDF student documents. **DO NOT EDIT!**
 * `reports_workspace/` - This is your workspace. You can edit all of the files within here. **THIS IS WHERE YOU SHOULD EDIT THE STUDENT'S PDF FILES**

Note: Output should look simililar to the following:

~~~~
$ python setup.py Archive.zip 
68335-65425 - Li Do - 20000000
68367-65425 - Lu Sen - 20000000
68389-65425 - Li Yinbo - 20000000
68399-65425 - Lang Chen - 20000000
68435-65425 - Aminpour Sherwin - 20000000
68455-65425 - Li Jiazhou - 20000000
~~~~

 * Note 1: Use 'python setup.py clean' to restart in case of problems.
 * Note 2: If 'clean' does not work, folders created in Third Step may need to be manually deleted. **DO NOT DELETE ANYTHING YOU HAVE EDITED!**

## Fourth Step: How/Where to Start Editing
 * Go into one of the student's folders in `reports\_workspace/` directory
 * Open the `(commented)*.pdf` with a PDF editor that you can write comments with
    * [Foxit Reader](https://www.foxitsoftware.com/products/pdf-reader/) is Free and can do this
 * Open `evaluation.xls`
 * Comment in the PDF and fill out ALL the sheets within `evaluation.xls`
    * **MAKE SURE THAT ALL TEXT IN `evaluation.xls` IS LATEX FRIENDLY**
 * To check if you have any LaTex errors, run the command `python finalize.py` in the student's directory. All LaTex errors will be outputted here.

## Fifth Step: How Do I Upload My Feedback Quickly?
 1. Run `python finalizeAll.py`
 1. Run `python collectAll.py`
 1. Go into `evaluations/` and put all of the \*/zip files into a ZIP folder `evaluations.zip`
    * It should look identical to the file structure of the downloaded ZIP file from the Third Step
 1. Go to Learn and upload `evaluations.zip` using **Add Feedback Files**
 1. Wait till the grades are entered into the system and then select all submissions and click **Publish Feedback**

### Entering Grades (Option 1)
 1. Go into the dropbox and click on any student's submission
 1. Manually enter in each student's grade using the **Evaluate** function and then by pressing **Next**
 1. Release all grades at once by selecting all submissions and using the **Publish Feedback** button

### Entering Grades (Option 2)
**Known Bug:** When you upload you might get a million emails saying your mark has been updated, 
go to learn and turn off email-notifications for that course before uploading. 
No other student or TA will get those emails.

 1. Go to Learn and download the gradebook:
    * On the main bar for the class: "Course Home", "Course Material", ..., "Grades" (click on **Grades**)
    * Sub-bar menu: click on **Enter Grades**
    * Click on **Export**
        * All Users
        * Both
        * Points Grade
        * NO CHECKMARKS
        * Grade Item = Work Report
    * Click **Export to CSV**
 1. You run this program `python collectGrades.py FILE_ABOVE.csv`
 1. A new CSV file will be created with all of the marks populated. NOTE: look for errors in the output
 1. Upload this new CSV file and all of the grades will be entered into the system automatically

## Sixth Step: (Optional) How to Mark Resubmitted Reports?
 1. Run `python collectResubmits.py`
 1. This collects all reports that have RESUBMIT as their final score and puts them in the folder `resubmits/`. Each folder contains:
    * feedback/
    * resubmitted_report/
    * (commented)***.pdf
    * evaluation.xls
    * finalize.py
 1. Once the resubmit window is closed (ask the professor when they want it), download all the reports that got resubmitted into Learn (the student uses the same dropbox).
 1. Put each resubmitted report into the the student's folder, under the sub-folder `resubmitted_report`
 1. This sub-folder contains the same files as when marking the original:
    * feedback/
    * evaluation.xls
    * finalize.py
 1. Mark each resubmitted report and then manually update their grade in Learn

---

## What does Each Script Do?
 * **collectAll.py**
    * This script collects all of the feedback.pdf's and the commented work term reports and puts them into a \*.zip file for each student
    * this allows for very quick uploading of feedback)
    * Stores the zip file into `evaluations/`
 * **collectGrades.py**
    * You download the CSV grades for the dropbox (and only the 1 dropbox), 
        make sure to select ID (should be the same as in the folder names) for the first column, 
        and grade for the second column, no other columns
    * You run this program `python collectGrades.py FILE_ABOVE.csv`
    * A new CSV file will be created with all of the marks populated. NOTE: look for errors in the output
    * Upload this new CSV file and all of the grades will be entered into the system automatically
    * **Known Bug:** When you upload you might get a million emails saying your mark has been updated, 
        go to learn and turn off email-notifications for that course before uploading. 
        No other student or TA will get those emails.
 * **collectResubmits.py**
    * This collects all reports that have RESUBMIT as their final score and puts them in the folder `resubmits/`. Each folder contains:
        * feedback/
        * resubmitted_report/
        * (commented)***.pdf
        * evaluation.xls
        * finalize.py
 * **count\_completed.py**
    * Counts the number of reports that have been finalized and contain a final score
    * Outputs stats and a nice image (generated with RScript) to show the distribution of grades
    * Useful for seeing progress as you are marking and for giving a report to the professor at the end of marking
 * **finalizeAll.py**
    * Run this to create the feedback files for all students
    * This script goes through all of the directories in `reports\_workspace/` and runs the script `python finalize.py`
 * **setup.py**
    * Used to unpack a ZIP file of the downloaded work term reports from Learn
 * **update\_default\_response.py**
    * This script will replace all `evaluation.xls`, `finalize.py`, and `feedback/` in the folder `reports\_workspace/` with the updated version in `default\_response/`
 * **update\_evaluation.py**
    * This script will replace all `evaluation.xls` in the folder `reports\_workspace/` with the updated version in `default\_response/`
 * **update\_finalize.py**
    * This script will replace all `finalize.py` in the folder `reports\_workspace/` with the updated version in `default\_response/`