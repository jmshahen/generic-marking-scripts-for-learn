import os
import sys
import re
import glob

from datetime import date

import colorama
from colorama import Fore, Back, Style

# Make sure the working directory is the same as the script file
os.chdir(os.path.dirname(os.path.realpath(__file__)))


class CurrentFolder:
    maxl = 12
    i = 1
    txt = "bb"
    def update(self, pbar):
        return (str(self.i) + ". " + self.txt).ljust(self.maxl + 5)

def main():
    if len(sys.argv) != 2 or not sys.argv[1].endswith(".csv"):
        print "Usage: python collectGrades.py csvfile"
        exit(0)
    
    # Initialize colorama
    colorama.init(autoreset=True)
    
    # Check if file exists
    file = sys.argv[1]
    if not os.path.isfile(file):
        print "File '"+ file +"' could not be found!"
        exit(1)
    
    # read file contents
    f = open(file, 'r')
    lines = f.readlines()
    f.close()
    
    # initialize variables
    i=0
    found=0
    not_found=0
    duplicates=0
    new_csv=list()
    colour_grades = {
        'No Submission': Fore.RED,
        'Unacceptable': Fore.RED,
        'Resubmit': Fore.YELLOW,
        'Satisfactory': Fore.YELLOW,
        'Very Good': Fore.MAGENTA,
        'Excellent': Fore.CYAN,
        'Outstanding': Fore.GREEN + Style.BRIGHT}
    percent_grades = {
        'No Submission': '0',
        'Unacceptable': '38',
        'Resubmit': '38',
        'Satisfactory': '65',
        'Very Good': '75',
        'Very good': '75',
        'Excellent': '89',
        'Outstanding': '95'}
    
    # verify there is only 1 spot available to put the numerical grade
    for l in lines:
        if i == 0:
            new_csv.append(l) # copy the csv header over
            i += 1
            continue
        
        l_s=l.split(",,")
        if not len(l_s) == 2:
            print "Zero or More than one spot is free on this line:\n" + l
            exit(1)
        
        # extract information
        id=l[1:l.index(",")]
        print id,
        
        f_id = glob.glob("./reports_workspace/*"+id+"*/Final Mark - *.txt".replace("/", os.sep))
        if len(f_id) == 0:
            print(Fore.YELLOW + Style.BRIGHT + "No Folder and File could be found! Have you FINALIZED all the reports?")
            not_found += 1
            continue
        elif len(f_id) > 1:
            print(Fore.YELLOW + Style.BRIGHT + "Multiple Final Marks were found! You will need to manually correct this!")
            duplicates += 1
            continue
        
        found += 1
        result = re.search('Final Mark - (.*).txt', f_id[0])
        mark = result.group(1)
        print(colour_grades[mark] + mark + " (" + percent_grades[mark] + " %)")
        
        # add the numerical grade to the csv line
        new_csv.append(l_s[0] + "," + percent_grades[mark] + "," + l_s[1])
        
    print
    print
    print "Total found:      " + str(found)
    print "Total not found:  " + str(not_found)
    print "Total duplicates: " + str(duplicates)
    print "Total processed:  " + str(len(lines)-1)
    
    filename = 'Grades (' + str(date.today()) + ").csv"
    f=open(filename, 'w')
    for l in new_csv:
        f.write(l)
    f.close()
    print "Results Stored:   '" + filename + "'"

main()