import os
import sys
import shutil
import glob
import time
import colorama
from colorama import Fore, Back, Style

# Make sure the working directory is the same as the script file
os.chdir(os.path.dirname(os.path.realpath(__file__)))

def main():
    # Initialize colorama
    colorama.init(autoreset=True)
    
    all = False
    if len(sys.argv) == 2:
        if sys.argv[1] == "all":
            all = True
        else:
            print "Usage: python update_finalize.py [all]"
            exit(0)

    default_src = "./default_response/evaluation.xls".replace("/", os.sep)
    evals = glob.glob("./reports_workspace/*/evaluation.xls".replace("/", os.sep))
    
    update_count = 0

    for eval in evals:
        path = os.path.dirname(eval)
        test = glob.glob(path + os.sep + 'Final Mark - *.txt')
        
        # Skip folders that already have been marked
        if len(test) != 0:
            print(Style.DIM + 'Skipping: ' + test[0])
            continue
        
        print(Style.BRIGHT + eval)
        if all == True:
            foo = 'y'
        else:
            foo=raw_input('Continue? (y/n): ')
        
        if foo == 'y' or foo == 'Y' or foo == "":
            print(Fore.RED + 'Deleting' + Fore.WHITE + ' ...'),
            os.remove(eval)
            print(Fore.YELLOW + 'Deleted' + Fore.WHITE + ' ...'),
            time.sleep(0.1)
            shutil.copyfile(default_src, eval)
            print(Fore.GREEN + 'Updated')
            update_count += 1
        else:
            print 'Skipped'
        
    
    print 
    print str(update_count) + "/" + str(len(evals))

main()