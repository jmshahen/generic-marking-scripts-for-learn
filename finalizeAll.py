import os
import sys
import subprocess
import zipfile
import fnmatch
import shutil
import progressbar
import glob

# Make sure the working directory is the same as the script file
os.chdir(os.path.dirname(os.path.realpath(__file__)))

os.chdir("./reports_workspace/")

dirs = os.walk('.').next()[1]
i=1

def cleanUp():
    searches=['*.zip', '*.txt']
    for s in searches:
        files=glob.glob(s)
        for f in files:
            os.remove(f)

class CurrentFolder:
    maxl = 12
    i = 1
    txt = "bb"
    def update(self, pbar):
        return (str(self.i) + ". " + self.txt).ljust(self.maxl + 5)

maxlength = max(len(s) for s in dirs)
cf = CurrentFolder()
cf.maxl = maxlength
widgets = [cf, progressbar.Bar(), '>>> ',progressbar.Percentage(),' ', progressbar.ETA()]
pbar = progressbar.ProgressBar(widgets=widgets, maxval=len(dirs))
# maybe do something
pbar.start()

for dir in dirs:
    cf.txt = dir
    cf.i = i
    pbar.update(i)
    os.chdir("./" + dir)
    
    cleanUp()
    
    FNULL = open(os.devnull, 'w')
    subprocess.call(['python', 'finalize.py', 'quiet'], stdout=FNULL, stderr=subprocess.STDOUT)
    # os.system("python \"finalize.py\" quiet")
    os.chdir("./../")
    i=i+1
    
print
print
print "Total processed: " + str(len(dirs))
