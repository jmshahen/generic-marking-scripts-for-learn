import zipfile,os.path,os,shutil,sys
import glob
import time
import colorama
from colorama import Fore, Back, Style

# Make sure the working directory is the same as the script file
os.chdir(os.path.dirname(os.path.realpath(__file__)))

def copy_files(path):
    files = glob.glob(os.path.abspath("reports_workspace") + "/*/Award Consideration.txt".replace("/", os.path.sep))
    for name in files:
        dirname = name.split(os.path.sep)[-2]
        print("Copying folder: " + dirname)
        if os.name == 'nt':
            # windows hack for long file names
            src = unicode('\\\\?\\' + os.path.abspath('reports_workspace/') + '\\' + dirname)
        else:
            src = os.path.abspath('reports_workspace/') + os.path.sep + dirname
        shutil.copytree(src, path + os.path.sep + dirname)
        

def clean():
    shutil.rmtree('./reports_original')
    shutil.rmtree('./reports_workspace')
    
def main():
    # Initialize colorama
    colorama.init(autoreset=True)
    
    if not os.path.isdir("./award_consideration"):
        os.makedirs("./award_consideration")
    
    if os.name == 'nt':
        # windows hack for long file names
        path = unicode('\\\\?\\' + os.path.abspath('award_consideration/'))
    else:
        path = os.path.abspath('award_consideration/')
    print(Fore.GREEN + 'Storing the files in: ' + path)
    copy_files(path)

main()