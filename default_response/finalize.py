import os
import subprocess
import zipfile
import fnmatch
import shutil
import xlrd
import sys

import colorama
from colorama import Fore, Back, Style

# Make sure the working directory is the same as the script file
os.chdir(os.path.dirname(os.path.realpath(__file__)))

# Initialize colorama
colorama.init(autoreset=True)

quiet = False
if len(sys.argv) == 2 and sys.argv[1] == "quiet":
    quiet = True

percent_grades = {
        'No Submission': '0',
        'Unacceptable': '38',
        'Resubmit': '38',
        'Satisfactory': '65',
        'Very Good': '75',
        'Very good': '75',
        'Excellent': '89',
        'Outstanding': '95'}
    
os.chdir("./feedback/")

workbook = xlrd.open_workbook("../evaluation.xls")
# Checks for required sheet names
required_names = ['Checklist', 'Technical Communication', 'Technical Content', 'Final Evaluation']
sheet_names = workbook.sheet_names()
for worksheet_name in required_names:
    if worksheet_name not in sheet_names:
        print(Fore.RED + '[Critical Error] unable to find sheet "' + worksheet_name + '" in the file "../evaluation.xls"')
        sys.exit(1)

############################################################################################
############################################################################################
# Creates the formatting Checklist in format.tex
checklist = workbook.sheet_by_name('Checklist')
frmttex = open("format.tex", "w")
frmttex.write("\\section{General formatting}\n")
frmttex.write("\\begin{enumerate}\n")

checked = "\\item \\makebox[0pt][l]{$\\square$}\\raisebox{.15ex}{$\\checkmark$}\\hspace{2 mm}"
not_checked = "\\item \\makebox[0pt][l]{$\\square$}\\raisebox{.15ex}{\\color{red}$\\times$}\\hspace{2 mm}"

num_rows = checklist.nrows - 1
if checklist.ncols < 2:
    print(Fore.RED + '[Critical Error] Checklist does not contain enough columns')
    sys.exit(2)

curr_row = 0
while curr_row <= num_rows:
    if curr_row == 0:
        curr_row = curr_row + 1
        continue # skip the first row
    
    # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank
    found_type = checklist.cell_type(curr_row, 0)
    criteria_type = checklist.cell_type(curr_row, 1)
    
    if found_type == 1 and criteria_type == 1:
        found_value = checklist.cell_value(curr_row, 0)
        criteria_value = checklist.cell_value(curr_row, 1)
        
        if found_value.lower() == 'yes':
            frmttex.write(checked + criteria_value + "\n")
        else:
            frmttex.write(not_checked + criteria_value + "\n")
    elif criteria_type == 0:
        pass # skip these rows
    else:
        print(Fore.RED + '[Critical Error] row ' + str(curr_row) + ' on "Checklist" there exists a non string')
        sys.exit(2)
    # Go to the next row
    curr_row = curr_row + 1

frmttex.write("\\end{enumerate}\n")
frmttex.close()

############################################################################################
############################################################################################
# Technical Communication
tech_comm = workbook.sheet_by_name('Technical Communication')

num_rows = tech_comm.nrows - 1
if tech_comm.ncols < 2:
    print(Fore.RED + '[Critical Error] Technical Communication does not contain enough columns')
    sys.exit(3)

curr_row = 0
while curr_row <= num_rows:
    if curr_row == 0:
        curr_row = curr_row + 1
        continue # skip the first row
    
    # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank
    criteria_type = tech_comm.cell_type(curr_row, 0)
    comment_type = tech_comm.cell_type(curr_row, 1)
    
    if criteria_type == 1 and comment_type == 1:
        criteria_value = tech_comm.cell_value(curr_row, 0)
        comment_value = tech_comm.cell_value(curr_row, 1)
        
        if criteria_value.lower() == 'grammar':
            efile = open("./evaluations/grammar.txt",'w')
            efile.write(comment_value)
            efile.close()
        elif criteria_value.lower() == 'structure':
            efile = open("./evaluations/structure.txt",'w')
            efile.write(comment_value)
            efile.close()
        elif criteria_value.lower() == 'cohesion/fluency/clarity':
            efile = open("./evaluations/cohesion.txt",'w')
            efile.write(comment_value)
            efile.close()
        else:
            print(Fore.YELLOW + '[WARNING] Found a unused row in "Technical Communication": ' + criteria_value + "\n" + comment_value)
    elif criteria_type == 0:
        pass # skip these rows
    else:
        print(Fore.RED + '[Critical Error] row ' + str(curr_row) + ' of "Technical Communication" there exists a non string')
        sys.exit(3)
    # Go to the next row
    curr_row = curr_row + 1

############################################################################################
############################################################################################
# Technical Content
tech_cont = workbook.sheet_by_name('Technical Content')

num_rows = tech_cont.nrows - 1
if tech_cont.ncols < 2:
    print(Fore.RED + '[Critical Error] Technical Content does not contain enough columns')
    sys.exit(4)

curr_row = 0
while curr_row <= num_rows:
    if curr_row == 0:
        curr_row = curr_row + 1
        continue # skip the first row
    
    # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank
    criteria_type = tech_cont.cell_type(curr_row, 0)
    comment_type = tech_cont.cell_type(curr_row, 1)
    
    if criteria_type == 1 and comment_type == 1:
        criteria_value = tech_cont.cell_value(curr_row, 0)
        comment_value = tech_cont.cell_value(curr_row, 1)
        
        if criteria_value.lower() == 'is the required background knowledge provided?':
            efile = open("./evaluations/background.txt",'w')
            efile.write(comment_value)
            efile.close()
        elif criteria_value.lower() == 'is the problem stated clearly?':
            efile = open("./evaluations/clarity.txt",'w')
            efile.write(comment_value)
            efile.close()
        elif criteria_value.lower() == 'is the problem\'s significance mentioned?':
            efile = open("./evaluations/significance.txt",'w')
            efile.write(comment_value)
            efile.close()
        elif criteria_value.lower() == 'does the author properly describe the domain, the environment, and the target audience?':
            efile = open("./evaluations/description.txt",'w')
            efile.write(comment_value)
            efile.close()
        elif criteria_value.lower() == 'are the design constraints and criteria well-defined?':
            efile = open("./evaluations/constraints-and-criterias.txt",'w')
            efile.write(comment_value)
            efile.close()
        elif criteria_value.lower() == 'are any alternatives offered by the author?':
            efile = open("./evaluations/alternatives_offer.txt",'w')
            efile.write(comment_value)
            efile.close()
        elif criteria_value.lower() == 'do the alternatives properly fit in the context?':
            efile = open("./evaluations/alternatives_exploration.txt",'w')
            efile.write(comment_value)
            efile.close()
        elif criteria_value.lower() == 'is the solution space explored in adequate depth?':
            efile = open("./evaluations/alternatives_context.txt",'w')
            efile.write(comment_value)
            efile.close()
        elif criteria_value.lower() == 'are the assumptions, if any, reasonable?':
            efile = open("./evaluations/assumptions.txt",'w')
            efile.write(comment_value)
            efile.close()
        elif criteria_value.lower() == 'is the evaluation fair to all the alternatives?':
            efile = open("./evaluations/fairness.txt",'w')
            efile.write(comment_value)
            efile.close()
        elif criteria_value.lower() == 'is the evaluation based on the criteria?':
            efile = open("./evaluations/evaluation_criteria.txt",'w')
            efile.write(comment_value)
            efile.close()
        elif criteria_value.lower() == 'is the analysis sound and correct?':
            efile = open("./evaluations/soundness.txt",'w')
            efile.write(comment_value)
            efile.close()
        elif criteria_value.lower() == 'is the conclusion based on the observations (i.e. honest)?':
            efile = open("./evaluations/observation.txt",'w')
            efile.write(comment_value)
            efile.close()
        elif criteria_value.lower() == 'are the trade-offs considered?':
            efile = open("./evaluations/tradeoff.txt",'w')
            efile.write(comment_value)
            efile.close()
        else:
            print(Fore.YELLOW + '[WARNING] Found a unused row in "Technical Content": ' + criteria_value + "\n" + comment_value)
    elif criteria_type == 0:
        pass # skip these rows
    else:
        print(Fore.RED + '[Critical Error] row ' + str(curr_row) + ' of "Technical Content" there exists a non string')
        sys.exit(4)
    # Go to the next row
    curr_row = curr_row + 1

############################################################################################
############################################################################################
# Final Evaluation
final_eval = workbook.sheet_by_name('Final Evaluation')

num_rows = final_eval.nrows - 1
if final_eval.ncols < 2:
    print(Fore.RED + '[Critical Error] Technical Content does not contain enough columns')
    sys.exit(5)

award_consideration = False
confidential = False
curr_row = 0
while curr_row <= num_rows:
    # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank
    criteria_type = final_eval.cell_type(curr_row, 0)
    comment_type = final_eval.cell_type(curr_row, 1)
    criteria_value = final_eval.cell_value(curr_row, 0)
    comment_value = final_eval.cell_value(curr_row, 1)
    
    if criteria_type == 1 and comment_type == 1:
        if criteria_value.lower() == 'final mark':
            efile = open("./evaluations/final.txt",'w')
            final_mark = comment_value.lower().capitalize()
            if final_mark == 'Very good':
                final_mark = 'Very Good'
            if final_mark == 'No submission':
                final_mark = 'No Submission'
            efile.write(final_mark)
            efile.close()
        elif criteria_value.lower() == "overall comments":
            efile = open("./evaluations/comments.txt",'w')
            efile.write(comment_value)
            efile.close()
        elif criteria_value.lower() == "award consideration":
            if comment_value.lower() == 'yes':
                award_consideration = True
        elif criteria_value.lower() == "confidential":
            if comment_value.lower() == 'yes':
                confidential = True
        else:
            print(Fore.YELLOW + '[WARNING] Found a unused row in "Final Evaluation": ' + criteria_value + "\n" + comment_value)
    elif criteria_type == 0:
        pass # skip these rows
    else:
        print(Fore.RED + '[Critical Error] row ' + str(curr_row) + ' of "Final Evaluation" there exists a non string: "' +
            criteria_value + '" (' + str(criteria_type) +'), "' + comment_value +'" (' + str(comment_type) +')')
        # sys.exit(5)
    # Go to the next row
    curr_row = curr_row + 1

############################################################################################
############################################################################################

print("\n\n############################################################################################")
print('############################################################################################')
# Create the PDF file
subprocess.call(["pdflatex", "feedback.tex"])
subprocess.call(["pdflatex", "feedback.tex"])

print("\n############################################################################################")
print("############################################################################################\n\n")

os.chdir('..')

filename = ''
for file in os.listdir("."):
    if fnmatch.fnmatch(file, "(commented)*"):
        filename = file

shutil.copy("feedback/feedback.pdf", '.')

final_mark_file = "Final Mark - " + final_mark +".txt"
efile = open(final_mark_file,'w')
efile.write("Final Mark: " + final_mark + " (" + percent_grades[final_mark] + " %)")
efile.close()



colour_grades = {
        'No Submission': Fore.RED,
        'Unacceptable': Fore.RED,
        'Resubmit': Fore.YELLOW,
        'Satisfactory': Fore.YELLOW,
        'Very Good': Fore.MAGENTA,
        'Excellent': Fore.CYAN,
        'Outstanding': Fore.GREEN + Style.BRIGHT}
print(colour_grades[final_mark] + 'Final Mark: ' + final_mark)

arch = zipfile.ZipFile("evaluation.zip", 'w')
# add to zip file
arch.write("feedback.pdf")
if not filename == '':
    arch.write(filename) # commented pdf
arch.write(final_mark_file)

if award_consideration == True:
    if confidential == True:
        print(Fore.YELLOW + "[WARNING] Giving an Award Consideration to a Confidential Report")
    print('Award Consideration: ' + Fore.GREEN + 'Given')
    efile = open('Award Consideration.txt','w')
    efile.write("Award Consideration")
    efile.close()
    # add to zip file
    arch.write('Award Consideration.txt')
else:
    print('Award Consideration: None')

if confidential == True:
    efile = open('Confidential.txt','w')
    efile.write("Confidential Report")
    efile.close()
    # add to zip file
    arch.write('Confidential.txt')

# Close Zip file
arch.close();

# os.remove(filename)
os.remove("feedback.pdf")
os.remove("feedback/feedback.log")
os.remove("feedback/feedback.aux")
os.remove("feedback/feedback.out")

if quiet == False:
    print
    raw_input("Done!\n")