import os
import sys
import subprocess
import glob
import time
import re
import math
import colorama
from colorama import Fore, Back, Style

# Make sure the working directory is the same as the script file
os.chdir(os.path.dirname(os.path.realpath(__file__)))

def main():
    print "Running count_completed.py"
    print
    # Change the order of the PNG
    reverseOrder = False
    if len(sys.argv) == 2:
        if sys.argv[1] in ("reverse", "reversed"):
            reverseOrder = True
            print "Reversing the Order of the PNG"
    
    colorama.init(autoreset=True)
    
    evals = glob.glob("./reports_workspace/*/finalize.py".replace("/", os.sep))
    total_reports = len(evals)
    
    evals = glob.glob("./reports_workspace/*/Final Mark - *.txt".replace("/", os.sep))
    total_marked = len(evals)
    
    glob_marks = {
        'No Submission': Fore.RED,
        'Unacceptable': Fore.RED,
        'Resubmit': Fore.RED,
        'Satisfactory': Fore.YELLOW,
        'Very Good': Fore.MAGENTA,
        'Excellent': Fore.CYAN,
        'Outstanding': Fore.GREEN + Style.BRIGHT}
    glob_count = {
        'No Submission': 0,
        'Unacceptable': 0,
        'Resubmit': 0,
        'Satisfactory': 0,
        'Very Good': 0,
        'Excellent': 0,
        'Outstanding': 0}
    
    times = list()
    names = list()
    marks = list()
    
    print("Done initialization")
    
    for ev in evals:
        times.append(os.path.getctime(ev))
        result = re.search('Final Mark - (.*).txt', ev)
        if result.group(1) == 'Very good':
            mark = 'Very Good'
        else:
            mark = result.group(1)
        marks.append(mark)
        names.append(ev.replace("Final Mark - " + marks[-1] + ".txt", "").replace("reports_workspace", "").replace(os.sep, "").replace(".", ""))
        glob_count[mark] += 1
        
    zipped = zip(names, times, marks)
    zipped.sort(key = lambda t: t[1])
    for z in zipped:
        print("[" + Style.BRIGHT + str(time.ctime(z[1])) + Style.RESET_ALL + "] " + glob_marks[z[2]] + z[2].ljust(12) + Fore.RESET + " - " +z[0])
        
    print
    print(Style.BRIGHT + 'Totals:')
    print "Total Marked : " + str(total_marked) + " (" + str(100* total_marked / total_reports) + "%)"
    print "Total Reports: " + str(total_reports)
    
    print
    print(Style.BRIGHT + "Award Consideration:")
    evals = glob.glob("./reports_workspace/*/Award Consideration.txt".replace("/", os.sep))
    total_awards = 0
    for eval in evals:
        print "\t" + eval.replace("Award Consideration.txt", "").replace("reports_workspace", "").replace(os.sep, "").replace(".", "")
        total_awards += 1
        
    print
    print(Style.BRIGHT + 'Hitogram:')
    ordering = ['Outstanding', 'Excellent', 'Very Good', 'Satisfactory', 'Resubmit', 'Unacceptable', 'No Submission']
    maxstrlen = max(len(s) for s in ordering)
    for o in ordering:
        print(glob_marks[o] + o.ljust(maxstrlen) + ": " + str(glob_count[o]).rjust(3) + ' (' + str(100 * glob_count[o] / total_marked).rjust(2) + '%)')

    width=1024
    height=720
    ylim = math.ceil((max(glob_count.values())+0)/5.0)*5
    
    
    graphOrder = "x<-c({nosub},{unac},{resub},{sat},{vgood},{exc},{out},{ac})\n"
    graphOrderArr = reversed(range(1,9)) #[8,1]
    graphColours = "'orangered','orangered','orangered','palegreen4','palegreen3','palegreen2','palegreen1','forestgreen'"
    if reverseOrder == True:
        graphOrder = "x<-c({ac},{out},{exc},{vgood},{sat},{resub},{unac},{nosub})\n"
        graphOrderArr = range(1,9) #[1,8]
        graphColours = "'forestgreen','palegreen1','palegreen2','palegreen3','palegreen4','orangered','orangered','orangered'"
    
    r_graph = (graphOrder + 
        "midpoints <- barplot(x,  ylim=c(0, {ylim}), ylab='Number of Reports'," +
        "las=2, cex.lab=2, cex.names=1.5, cex.axis=1.5, " +
        "col=c({colours}))").format(
            ac=total_awards, out=glob_count['Outstanding'], exc=glob_count['Excellent'], vgood=glob_count['Very Good'], 
            sat=glob_count['Satisfactory'], resub=glob_count['Resubmit'], unac=glob_count['Unacceptable'], 
            nosub=glob_count['No Submission'], ylim=ylim, colours=graphColours)
    
    efile = open('grade_distribution.r','w')
    efile.write(("png(filename='grade_distribution.png', width={width}, height={height})\n".format(width=width, height=height)) +
        # c(bottom, left, top, right)
        "par(oma = c(0, 2, 2, 0)+ 0.1)\n" +
        "par(mar = c(7, 5, 4, 0) + 0.1)\n" +
        "par(xpd=NA)\n" + #don't clip within the figure
        r_graph + "\n" + 
        ("title(main='Distribution of Grades ({marked}/{total})', line=3, cex.main=2.5)\n".format(marked=total_marked, total=total_reports)) +
        "text(midpoints, x, pos=3, labels=sprintf('%d (%.1f%%)', x, 100 * x/" + str(total_reports) +"), cex=1.5)\n" +
        #"mtext(text='Number of\\nReports', cex=1.5, line=0, at=0, side=2)\n" +
        #"text(midpoints, par('usr')[3]-1, srt=0, cex=1.5, labels=c('Award Consideration', 'Outstanding', 'Excellent', 'Very Good', 'Satisfactory', 'Unacceptable'))\n" +
        ("mtext(text='Award\\nConsideration', at=midpoints[{0}], side=1, line=2, cex=1.5)\n"+
        "mtext(text='Outstanding', at=midpoints[{1}], side=1, line=1, cex=1.5)\n"+
        "mtext(text='Excellent', at=midpoints[{2}], side=1, line=1, cex=1.5)\n"+
        "mtext(text='Very Good', at=midpoints[{3}], side=1, line=1, cex=1.5)\n"+
        "mtext(text='Satisfactory', at=midpoints[{4}], side=1, line=1, cex=1.5)\n"+
        "mtext(text='Resubmit', at=midpoints[{5}], side=1, line=1, cex=1.5)\n"+
        "mtext(text='Unacceptable', at=midpoints[{6}], side=1, line=1, cex=1.5)\n"+
        "mtext(text='No\\nSubmission', at=midpoints[{7}], side=1, line=2, cex=1.5)\n").format(*graphOrderArr)+
        "graphics.off()")
    efile.close()
    
    print("Creating graphical version now (Requires Rscript to be on the PATH)")
    print('## STARTED #################################################################################')
    subprocess.call(["Rscript", "grade_distribution.r"])
    print('## FINISHED ################################################################################')
    print("Image created: grade_distribution.png")

main()
