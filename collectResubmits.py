import zipfile,os.path,os,shutil,sys
import glob
import time
import colorama
from colorama import Fore, Back, Style

# Make sure the working directory is the same as the script file
os.chdir(os.path.dirname(os.path.realpath(__file__)))

def copy_files(path):
    files = glob.glob(os.path.abspath("reports_workspace") + "/*/Final Mark - Resubmit.txt".replace("/", os.path.sep))
    for name in files:
        dirname = name.split(os.path.sep)[-2]
        print("Copying folder: " + dirname)
        if os.name == 'nt':
            # windows hack for long file names
            src = unicode('\\\\?\\' + os.path.abspath('reports_workspace/') + '\\' + dirname)
        else:
            src = os.path.abspath('reports_workspace/') + os.path.sep + dirname
        shutil.copytree(src, path + os.path.sep + dirname)
        
        new_path = path + os.path.sep + dirname + os.path.sep + 'resubmitted_report' + os.path.sep
        os.makedirs(new_path)
        shutil.copy("./default_response/evaluation.xls", new_path + "evaluation.xls")
        shutil.copy("./default_response/finalize.py", new_path + "finalize.py")
        shutil.copytree("./default_response/feedback", new_path + "feedback")
        

def clean():
    shutil.rmtree('./reports_original')
    shutil.rmtree('./reports_workspace')
    
def main():
    # Initialize colorama
    colorama.init(autoreset=True)
    
    if not os.path.isdir("./resubmits"):
        os.makedirs("./resubmits")
    
    if os.name == 'nt':
        # windows hack for long file names
        path = unicode('\\\\?\\' + os.path.abspath('resubmits/'))
    else:
        path = os.path.abspath('resubmits/')
    print(Fore.GREEN + 'Storing the files in: ' + path)
    copy_files(path)

main()